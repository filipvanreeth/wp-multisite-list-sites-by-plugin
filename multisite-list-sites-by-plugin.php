<?php
/*
 * Plugin Name: List Sites by Plugin in a Multisite Network
 * Plugin URI: https://example.com/my-plugin/
 * Description: Lists all sites where plugins are active in a Multisite network.
 * Version:  0.1.0
 * Requires at least: 6.0
 * Requires PHP:      7.4
 * Author: Filip Van Reeth
 * Author URI: https://filipvanreeth.com
 * License: GPL v2 or later
 * License URI:
 * Update URI:
 * Text Domain: multisite-list-sites-by-plugin
 * Domain Path: /languages
 * Network: true
 */

use FilipWP\Multisite_List_Sites_By_Plugin\CLI_Commands;
use FilipWP\Multisite_List_Sites_By_Plugin\Sites_By_Plugin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Define constants.
define( 'MULTISITE_LIST_SITES_BY_PLUGIN_DIR_URL', plugin_dir_url( __FILE__ ) );
define( 'MULTISITE_LIST_SITES_BY_PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ) );
define( 'MULTISITE_LIST_SITES_BY_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
/**
 * Initializes the plugin.
 */
class Multisite_List_Sites_By_Plugin {

	private static $instance = null;

	private function __construct() {
		if ( ! $this->is_multisite() ) {
			add_action( 'admin_notices', array( $this, 'no_multisite_admin_notice' ) );
			deactivate_plugins( MULTISITE_LIST_SITES_BY_PLUGIN_BASENAME );

			return;
		}

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_filter( 'manage_plugins-network_columns', array( $this, 'add_plugin_columns' ) );
		add_action( 'manage_plugins_custom_column', array( $this, 'render_plugin_data' ), 10, 3 );
		add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );

		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			\WP_CLI::add_command( 'plugin', CLI_Commands::class );
		}
	}

	public static function get_instance() {
		if ( self::$instance === null ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function load_textdomain() {
		load_plugin_textdomain( 'multisite-list-sites-by-plugin', false, MULTISITE_LIST_SITES_BY_PLUGIN_DIR_PATH . 'languages/' );
	}

	private function is_multisite(): bool {
		return is_multisite();
	}

	public function is_no_multisite_admin_notice(): void {
		echo '<div class="error"><p>' . __( 'This plugin requires a multisite network.', 'multisite-list-sites-by-plugin' ) . '</p></div>';
	}

	public function enqueue_scripts(): void {
		$current_screen = get_current_screen();

		if ( $current_screen->id !== 'plugins-network' ) {
			return;
		}

		wp_enqueue_style( 'multisite-list-sites-by-plugin', plugin_dir_url( __FILE__ ) . 'assets/dist/css/admin-style.css' );
	}

	public function add_plugin_columns( $columns ) {
		$columns['sites'] = __( 'Active on Sites', 'multisite-list-sites-by-plugin' );

		return $columns;
	}

	public function render_plugin_data( $column_name, $plugin_file, $plugin_data ) {
		if ( $column_name == 'sites' ) {
			$sites_by_plugin = new Sites_By_Plugin( $plugin_file );
			$sites = $sites_by_plugin->get_sites();

			if ( ! $sites ) {
				return '';
			}

			$output = '<ul class="multisite-sites-list-by-plugin">';

			foreach ( $sites as $site ) {
				switch_to_blog( $site->blog_id );
				$output .= sprintf( '<li><a href="%s">%s</a></li>', admin_url( 'plugins.php' ), home_url() );
				restore_current_blog();
			}

			$output .= '</ul>';

			echo $output;
		}
	}
}

( Multisite_List_Sites_By_Plugin::get_instance() );
