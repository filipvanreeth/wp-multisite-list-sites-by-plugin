<?php
namespace FilipWP\Multisite_List_Sites_By_Plugin;

use WP_CLI;

class CLI_Commands {

	/**
	 * List sites by plugin
	 *
	 * ## EXAMPLES
	 *
	 *     wp plugin list-sites
	 *
	 * @subcommand list-sites
	 */
	public function list_sites(): void {
		require_once ABSPATH . 'wp-admin/includes/plugin.php';

		$plugins = get_plugins();

		foreach ( $plugins as $plugin_file => $plugin_data ) {
			$is_plugin_active = is_plugin_active( $plugin_file );

			$sites_by_plugin = new Sites_By_Plugin( $plugin_file );

			$sites = $sites_by_plugin->get_sites();

			$plugin_slug = match ( true ) {
				$sites_by_plugin->is_mu_plugin() => basename( $plugin_file ),
				(bool) preg_match( '/^([^\/]+\..+)/', $plugin_file, $matches ) => pathinfo(
					basename( $plugin_file ),
					PATHINFO_FILENAME
				),
				default => dirname( $plugin_file ),
			};

			$plugin_status = match ( true ) {
				$sites_by_plugin->is_mu_plugin() => 'mu-plugin',
				$sites_by_plugin->is_dropin() => 'dropin',
				is_plugin_active_for_network( $plugin_file ) => 'active-network',
				! empty( $sites ) => 'active',
				default => 'inactive',
			};

			$rendered_sites = array_map(
				function ( $site ) {
					switch_to_blog( $site->blog_id );
					$url = home_url();
					restore_current_blog();
					return $url;
				},
				$sites
			);

			$rendered_sites = implode( ', ', $rendered_sites );

			$items[] = array(
				'name' => $plugin_slug,
				'status' => $plugin_status,
				'sites' => $rendered_sites,
			);
		}

		WP_CLI\Utils\format_items( 'table', $items, array( 'name', 'status', 'sites' ) );
	}
}
