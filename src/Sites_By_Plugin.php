<?php
namespace FilipWP\Multisite_List_Sites_By_Plugin;

class Sites_By_Plugin {


	private string $plugin_path;
	private array $sites = array();

	/**
	 * Constructor.
	 *
	 * @param string $plugin_path The path to the plugin.
	 */
	public function __construct( string $plugin_path ) {
		$this->plugin_path = $plugin_path;
		$this->sites = $this->fetch_sites_by_plugin_path();
	}

	/**
	 * Retrieves the path of the plugin.
	 *
	 * @return string The path of the plugin.
	 */
	public function get_plugin_path(): string {
		return $this->plugin_path;
	}

	/**
	 * Retrieves an array of sites that have the specified plugin path.
	 *
	 * @return array An array of sites that have the specified plugin path.
	 */
	private function fetch_sites_by_plugin_path(): array {
		require_once ABSPATH . 'wp-admin/includes/plugin.php';

		$plugin_path = $this->get_plugin_path();

		if ( is_plugin_active_for_network( $plugin_path ) ) {
			return array();
		}

		$all_plugins = get_plugins();

		$sites = array();

		foreach ( get_sites() as $site ) {
			switch_to_blog( $site->blog_id );

			if ( is_plugin_active( $plugin_path ) ) {
				$sites[] = $site;
			}

			restore_current_blog();
		}

		return $sites;
	}

	/**
	 * Retrieves an array of sites.
	 *
	 * @return array An array of sites.
	 */
	public function get_sites(): array {
		return $this->sites;
	}

	/**
	 * Checks if the plugin is a must-use (mu) plugin.
	 *
	 * @return boolean True if the plugin is a must-use (mu) plugin, false otherwise.
	 */
	public function is_mu_plugin(): bool {
		$mu_plugins = get_mu_plugins();
		return isset( $mu_plugins[ $this->get_plugin_path() ] );
	}

	/**
	 * Checks if the current plugin is a drop-in.
	 *
	 * @return boolean True if the plugin is a drop-in, false otherwise.
	 */
	public function is_dropin(): bool {
		$dropins = _get_dropins();
		return isset( $dropins[ $this->get_plugin_path() ] );
	}
}
